import "../../node_modules/bootstrap/dist/js/bootstrap.bundle"
import { toggle } from "slide-element"
import Lightpick from 'lightpick'
import fslightbox from 'fslightbox'
import Cookies from 'js-cookie'
import Swiper, { Navigation, Pagination } from 'swiper';
import awesome from "./awesome"

class App {
    constructor () {
        this.body = document.querySelector('body')
        this.site = document.querySelector('.site')
        this.burger_button = this.site.querySelector('[data-burger]')
        this.burger_button_close = this.site.querySelector('[data-burger-close]')
        this.class_burger = 'burger-open'
        this.class_overflow_hidden = 'overflow--hidden'
        this.child_nav = this.site.querySelectorAll('.sub-nav__child')
        this.slide_toogle_el = this.site.querySelectorAll('[data-slide-toggle-trigger]')
        this.eye_version_button = this.site.querySelector('[data-eye-version]')

        this.eventFN()
    }

    openBurger () {
        if (this.site) {
            this.site.classList.add(this.class_burger)
            if (window.matchMedia("(max-width: 1100px)").matches) {
                this.body.classList.add(this.class_overflow_hidden)
            }
            
        }
    }

    closeBurger () {
        if (this.site) {
            this.site.classList.remove(this.class_burger)
            if (window.matchMedia("(max-width: 1100px)").matches) {
                this.body.classList.remove(this.class_overflow_hidden)
            }
        }
    }

    callbackBurger () {
        if (this.site && this.site.classList.contains(this.class_burger)) {
            this.closeBurger()
        } else {
            this.openBurger()
        }
    }

    childNav () {
        if (this.child_nav) {
            for (let menu of Array.from(this.child_nav)) {
                let siblink_link = menu.previousElementSibling
                

                if (siblink_link) {
                    siblink_link.querySelector('span').insertAdjacentHTML('beforeend', `<span class="open-child"></div>`)
                    siblink_link.addEventListener('click', (e) => {
                        
                        if (window.matchMedia("(max-width: 1100px)").matches) {
                            e.preventDefault()
                            if (e.currentTarget.closest('li').classList.contains('is-open')) {
                                e.currentTarget.closest('li').classList.remove('is-open')
                            } else {
                                e.currentTarget.closest('li').classList.add('is-open')
                            }
                        }
                    })
                }
                

            }
        }
    }

    slideToogle () {
        if (this.slide_toogle_el) {
            for (let $el of Array.from(this.slide_toogle_el)) {

                $el.addEventListener('click', async (e) => {
                    const content_id = $el.getAttribute('data-slide-toggle-trigger')
                    const content = this.site.querySelector(`[data-slide-toggle-content=${content_id}]`)

                    if (content.style.display === 'none') {
                        // open
                        $el.classList.add('open')
                    } else {
                        // close
                        $el.classList.remove('open')
                    }
                    let is = await toggle(content)
                    
                })
            }
        }
    }

    datePicker () {
        const first = this.site.querySelectorAll('[data-datepicker-first]')

        if (first && first.length) {
            let options = {}
            for (let $el_date of first) {
                const first_id = $el_date.getAttribute('data-datepicker-first')
                const second = this.site.querySelector(`[data-datepicker-second="${first_id}"]`)
                

                if (second) {
                    options = {
                        field: $el_date,
                        secondField: second,
                        singleDate: false,
                    }
                } else {
                    options = {
                        field: $el_date,
                    }
                }

            }
            let picker = new Lightpick({ ...options, lang: 'ru', format: 'DD.MM.YYYY' });
        }
        
    }

    slideSearchInput () {
        const $button = this.site.querySelector('[data-slide-input]')

        if ($button) {
            const $input = $button.previousElementSibling
            
            $button.addEventListener('click', (e) => {
                if (!$button.closest('.search__form').classList.contains('open')) {
                    $button.closest('.search__form').classList.add('open')
                    $input.focus()
                } else {
                    $button.closest('.search__form').classList.remove('open')
                }
            })

            if ($input) {
                $input.addEventListener('focus', () => {
                    //$button.setAttribute('type', 'submit')
                })
                $input.addEventListener('blur', () => {
                    if ($input.value) {
                        $button.setAttribute('type', 'submit')
                    } else {
                        $button.closest('.search__form').classList.remove('open')
                        $button.setAttribute('type', 'button')
                    }
                    
                })
            }
        }
    }

    caraouselNews () {
        const $carousel = this.site.querySelectorAll('.carousel__wrapper .swiper')

        if ($carousel) {
            for (let $el of $carousel) {
                const swiper = new Swiper($el, {
                    modules: [Navigation, Pagination],
                    navigation: {
                        nextEl: '.carousel__wrapper .swiper-button-next',
                        prevEl: '.carousel__wrapper .swiper-button-prev',
                    },
                    spaceBetween: 20,
                    breakpoints: {
                        0: {
                            slidesPerView: 1
                        },
                        767: {
                            slidesPerView: 2
                        },
                        992: {
                            slidesPerView: 3
                        }
                    }
                });
            }
        }
        
    }

    tabs () {
        const tabs_buttons = document.querySelectorAll('[data-tabs]')
        const active_tab = (button) => {
            
            let name_el = button.dataset.tabs

            
                const contents = document.querySelectorAll(`[data-tabs-content="${name_el}"]`)

                if (contents) {
                    for (let content of contents) {
                        if (button.checked) {
                            content.style.display = 'block'
                        }
                        
                    }
                }

                if (button.hasAttribute('data-tabs-close')) {
                    if (contents) {
                        for (let content of contents) {
                            if (button.checked) {
                                content.style.display = 'none'
                            }
                        }
                    }
                }
        }

        if (tabs_buttons) {
            for (let button of tabs_buttons) {
                active_tab(button)
                button.addEventListener('change', (e) => {
                    active_tab(button)
                })
            }
        }
    }

    inputFile () {
        const input_files = document.querySelectorAll('input[type="file"]')

        if (input_files) {
            for (let input of input_files) {
                input.addEventListener('change', (e) => {
                    let text_el = input.closest('.fieldset__file').querySelector('span')
                    let files = e.target.files
                    if (files && files.length) {
                        text_el.innerHTML = Array.from(files).map(item => item.name).join(', ')
                    }
                    
                })
            }
        }
    }


    eventFN () {
        if (this.burger_button) {
            this.burger_button.addEventListener('click', () => this.callbackBurger())
        }

        if (this.burger_button_close) {
            this.burger_button_close.addEventListener('click', () => this.closeBurger())
        }

        this.childNav()
        this.slideToogle()
        this.datePicker()
        this.slideSearchInput()
        this.caraouselNews()
        this.tabs()
        this.inputFile()

        window.addEventListener('resize', () => { })
    }
}

window.app = new App()