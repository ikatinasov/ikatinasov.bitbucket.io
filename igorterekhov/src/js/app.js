import SmoothScroll from 'smoothscroll-for-websites'
import gsap from 'gsap'
import Rellax from 'rellax'
import slideToggle from '../js/slide'
import { ScrollToPlugin } from 'gsap/dist/ScrollToPlugin'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { Swiper, Navigation, Pagination, Mousewheel, Parallax } from 'swiper';
import 'fslightbox'
import curtains from './curtains'

const rellax = new Rellax('.rellax');



Swiper.use([Navigation, Pagination, Mousewheel, Parallax]);
gsap.registerPlugin(ScrollTrigger, ScrollToPlugin)


function ToggleNav () {
    let burger = document.querySelector('[data-burger]')
    let mobile_nav = document.querySelector('[data-burger-nav]')

    if (!burger || !mobile_nav) return false

    burger.addEventListener('click', (e) => {
        if (mobile_nav.classList.contains('active')) {
            mobile_nav.classList.remove('active')
            burger.classList.remove('active')
        } else {
            mobile_nav.classList.add('active')
            burger.classList.add('active')
        }
    })
} 
ToggleNav()

SmoothScroll({ 
    animationTime: 1000,
    stepSize: 40,
    
    accelerationDelta : 50,  // 50
    accelerationMax   : 3,   // 3

    pulseAlgorithm   : true,
    pulseScale       : 4,
    pulseNormalize   : 1,
})


function ExhibitionCarousel () {
    if (!document.querySelector('.exhibition-carousel')) {
        return false
    }
    var swiper = new Swiper(".exhibition-carousel .swiper-container", {
        
        //center: true,
        parallax: true,
        //slidesPerView: 'auto',
        //centeredSlides: true,
        scrollbar: {
            el: ".swiper-scrollbar",
            hide: true,
            snapOnRelease: true
        },
        mousewheel: {
            invert: false,
            releaseOnEdges: true,
            sensitivity: 5,
            thresholdTime: 1,
            //thresholdDelta: 2
        },
        pagination: {
            el: ".swiper-pagination",
            type: "progressbar",
        },
        breakpoints: {
            0:{
                spaceBetween: 15,
                slidesPerView: 1.5,
            },
            640: {
                spaceBetween: 140,
                slidesPerView: 2.5,
                //freeMode: true,
            }
        }
        
    });

    if (window.matchMedia("(max-width: 635px)").matches) {
        swiper.removeSlide(0)
    }

    
}


function test () {
    let top_line = document.querySelector('.top-line')
    if (document.querySelector('[data-gsap-unclear]')) {
        gsap.to('.site', {
            scrollTrigger: {
                trigger: "[data-gsap-unclear]",
                //markers: true,
                onToggle: self => {
                    const targetRect = self.trigger.offsetTop;
                    
                    if (self.isActive && self.direction !== -1) {
                        self.trigger.classList.add('animate')
                        
                        gsap.to(window, 1,{
                            scrollTo: '#unclear',
                            ease: 'slow(0.7, 0.7)'
                        });
    
                        top_line.classList.remove('invert-color')
                    } else {
                        
                    }
                }
            },
        })
    }
    

    

    const elements = ['[data-gsap-header]', '[data-gsap]', '[data-gsap-tesis-1]', '[data-gsap-tesis-2]', 
    '[data-gsap-tesis-3]', '[data-gsap-unclear-1]', '[data-gsap-unclear-2]', '[data-gsap-unclear-3]',
    '[data-gsap-unclear-4]', '[data-gsap-unclear-5]', '[data-gsap-unclear-6]', '[data-gsap-unclear-7]', 
    '[data-gsap-unclear-8]', '[data-gsap-unclear-9]', '[data-gsap-unclear-10]', '[data-gsap-unclear-11]',
    '[data-gsap-unclear-12]', '[data-gsap-unclear-13]', '[data-gsap-unclear-14]', '[data-gsap-unclear-15]',
    '[data-gsap-unclear-16]', '[data-gsap-unclear-17]', '[data-gsap-unclear-18]', '[data-gsap-unclear-19]', 
    '[data-gsap-unclear-20]', '[data-gsap-unclear-21]', '[data-gsap-unclear-22]', '[data-gsap-unclear-23]', 
    '[data-gsap-unclear-24]', '[data-gsap-unclear-25]','[data-gsap-exhibition]', '[data-gsap-history]',]

    elements.forEach(item => {
        if (document.querySelector(item)) {
            gsap.to('.site', {
                scrollTrigger: {
                    trigger: item,
                    //markers: true,
                    onToggle: self => {
                        if (self.isActive) {
                            self.trigger.classList.add('animate')
                        }
                    }
                },
            })
        }
    })
    const  elements_history = ['[data-gsap-history-1]', '[data-gsap-history-2]', '[data-gsap-history-3]', '[data-gsap-history-4]']
    elements_history.forEach(item => {
        if (document.querySelector(item)) {
            gsap.to('.site', {
                scrollTrigger: {
                    trigger: item,
                    //markers: true,
                    start: 'top center',
                    onToggle: self => {
                        if (self.isActive) {
                            self.trigger.classList.add('animate')
                        }
                    }
                },
            })
        }
    })

    /*const parallaxImage = ['[data-gsap-unclear-5]', '[data-gsap-tesis-img]', '[data-gsap-unclear-7-parallax]',
    '[data-gsap-unclear-10-parallax]', '[data-gsap-unclear-13-parallax]', '[data-gsap-unclear-15-parallax]', 
    '[data-gsap-unclear-17-parallax]', '[data-gsap-unclear-21-parallax]', '[data-gsap-unclear-23-parallax]'] 
    parallaxImage.forEach(item => {
        if (document.querySelector(item)) {
            gsap.to(`${item} img`, {
                yPercent: -30,
                ease: "none",
                //start: 'bottom bottom',
                scrollTrigger: {
                    trigger: item,
                    scrub: true,
                    //markers: true,
                }, 
            });
        }
    })*/
    
      

    

    if (window.matchMedia("(min-width: 992px)").matches) {
        gsap.to('.site', {
            scrollTrigger: {
                trigger: "[data-gsap-img]",
                //markers: {startColor: "black", endColor: "red", fontSize: "16px"},
                start: 'top 450px',
                end: 'center center',
                onToggle: self => {
                    const targetRect = self.trigger.offsetTop;
                    
                    if (self.isActive && self.direction === 1) {
                        self.trigger.classList.add('animate')
                        if (document.querySelector('[data-gsap-header]')) {
                            document.querySelector('[data-gsap-header]').classList.add('animate-img')
                        }

                        gsap.to(window, 1,{
                            scrollTo: targetRect,
                            ease: 'slow(0.7, 0.7)'
                        })
                    } else {
                        if (document.querySelector('[data-gsap-header]')) {
                            document.querySelector('[data-gsap-header]').classList.remove('animate-img')
                        }
                        
                    }
                },
            },
        })
    }
    if (document.querySelector('[data-gsap-header]')) {
        gsap.to('.site', {
            scrollTrigger: {
                trigger: "[data-gsap-header]",
                //markers: {startColor: "black", endColor: "red", fontSize: "16px"},
                end: 'bottom 75%',
                onToggle: self => {
                    if (!self.isActive && self.direction === 1) {
                        top_line.classList.add('invert-color')
    
                        if (document.querySelector('#tesis')) {
                            document.querySelector('#tesis').classList.add('animate-visible')
                            if (window.matchMedia("(min-width: 992px)").matches) {
                                gsap.to(window,{duration: 1, scrollTo: "#tesis"})
                            }
                        }
                        
                        if (window.matchMedia("(min-width: 992px)").matches) {
                            gsap.to('[data-gsap-header]', .5,{
                                ease: 'slow(0.7, 0.7)',
                                css: {
                                    scale: 2,
                                    opacity: 0
                                }
                            })
                        }
                        
                        
                    } else {
                        top_line.classList.remove('invert-color')
                        if (document.querySelector('#tesis')) {
                            document.querySelector('#tesis').classList.remove('animate-visible')
                        }
                        gsap.to('[data-gsap-header]', .5,{
                            ease: 'slow(0.7, 0.7)',
                            css: {
                                scale: 1,
                                opacity: 1
                            }
                        })
                    }
                },
            },
        })
    }
    
    
}



function SlideDown () {
    
    let buttons = document.querySelectorAll('.ages-list__item--title')

    if (!buttons) return false 

    buttons.forEach(item => {
        item.addEventListener('click', e => {
            const target = e.currentTarget
            const parent = target.closest('.ages-list__item')
            slideToggle(target.nextElementSibling, 300);
            if (parent.classList.contains('active')) {
                parent.classList.remove('active')
            } else {
                parent.classList.add('active')
            }
         })
    })
}

function SlideDownText () {
    const buttons = document.querySelectorAll('.block-text--name')

    if (!buttons) return false 

    if (window.matchMedia("(max-width: 645px)").matches) {
        buttons.forEach(item => {
            item.addEventListener('click', e => {
                const target = e.currentTarget
                const parent = target.closest('.block-text')
                slideToggle(target.nextElementSibling, 300);
                if (parent.classList.contains('active')) {
                    parent.classList.remove('active')
                } else {
                    parent.classList.add('active')
                }
             })
        })
    }
    
}


window.addEventListener('DOMContentLoaded', () => {
    ExhibitionCarousel()
    test()
    SlideDown()
    SlideDownText()
    window.scrollTo(0,0)
})

