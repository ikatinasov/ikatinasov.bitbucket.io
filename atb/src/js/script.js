import Swiper, { Navigation, Pagination, Grid } from 'swiper'
import { slideUp, slideDown, slideToggle } from 'tw-slide-toggle'
import fslightbox from 'fslightbox'
import IMask from 'imask'
import Cookies from 'js-cookie'
import { disablePageScroll, enablePageScroll } from 'scroll-lock';

class ATBLK {
    constructor () {
        this.site = document.querySelector('.site') || false
        this.burger = document.querySelector('.burger') || false
        this.top_line = document.querySelector('.top-line') || false
        this.nav_mobile = document.querySelector('.nav-mobile') || false
        this.header_carousel = document.querySelector('.header-carousel') || false
        this.arrange_leasing_carousel = document.querySelector('.arrange-leasing-carousel') || false
        this.search_target = document.querySelector('[data-search-target]') || false
        this.jobs = document.querySelectorAll('[data-job]') || false
        this.popup_links = document.querySelectorAll('[data-popup]')
        this.popup_links = document.querySelectorAll('[data-popup]')
        this.topOffset = 0

        this.Handlers ()
    }

    Handlers () {
        this.Burger()
        this.Carousels()
        this.Popups()
        this.SlideToogle()
        this.Jobs()
        this.MaskPhone()
        this.PolicyForm()
        this.FileInput()
        this.ImagesWrapperLinks()
        this.ValidateForm()
        this.SearchMobile()
        this.CookiesContainer()
        this.FixedTopLine()

        window.addEventListener('resize', (e) => {
            this.SlideToogle()
        })
        window.addEventListener('scroll', (e) => {
            this.FixedTopLine()
        })

    }

    Burger () {
        let topOffset = 0
        const handlers = {
            open: () => {
                if (this.burger && this.top_line && this.nav_mobile) {
                    this.burger.classList.add('active')
                    this.top_line.classList.add('active')
                    this.nav_mobile.classList.add('active')
                    topOffset = window.pageYOffset
                    this.topOffset = window.pageYOffset
                    document.querySelector('body').classList.add('overflow--hidden')
                    if (topOffset !== 0) {
                        document.querySelector('body').classList.add('fixed')
                    }
                    document.querySelector('body').style.top = `-${topOffset}px`
                } else {
                    console.error('not working open');
                }
            },
            close: () => {
                if (this.burger && this.top_line && this.nav_mobile) {
                    this.burger.classList.remove('active')
                    this.nav_mobile.classList.remove('active')
                    this.top_line.classList.remove('active')
                    document.querySelector('body').classList.remove('overflow--hidden')
                    document.querySelector('body').classList.remove('fixed')
                    
                    window.scrollTo(0, this.topOffset)
                    document.querySelector('body').style.top = ''
                    this.topOffset = 0
                } else {
                    console.error('not working close');
                }
            }
        }

        if (this.burger) {
            this.burger.addEventListener('click', (e) => {
                e.preventDefault()

                if (this.burger.classList.contains('active')) {
                   handlers.close()
                } else {
                   handlers.open()
                }
            })
        }

        return handlers
    }

    Carousels () {
        if (this.header_carousel) {
            new Swiper(this.header_carousel, {
                slidesPerView: 1,
                modules: [Navigation, Pagination],
                spaceBetween: 30,
                pagination: {
                    el: this.header_carousel.querySelector('.swiper-pagination'),
                    type: 'bullets',
                    clickable: true
                },
            });
        }
        if (this.arrange_leasing_carousel) {
            new Swiper(this.arrange_leasing_carousel, {
                modules: [Navigation, Pagination],
                pagination: {
                    el: this.arrange_leasing_carousel.querySelector('.swiper-pagination'),
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1
                    },
                    640: {
                        slidesPerView: 2
                    },
                    769: {
                        slidesPerView: 3
                    },
                    1000: {
                        slidesPerView: 5
                    }
                }
            });
        }

        let partners_carousel = document.querySelector('.partners-carousel')
        if (partners_carousel) {
            new Swiper(partners_carousel, {
                modules: [Navigation, Pagination],
                spaceBetween: 30,
                pagination: {
                    el: partners_carousel.querySelector('.swiper-pagination'),
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1
                    },
                    640: {
                        slidesPerView: 2
                    },
                    700: {
                        slidesPerView: 3
                    },
                    1000: {
                        slidesPerView: 5
                    }
                }
            });
        }

        let bracket_carousel = document.querySelector('.bracket-carousel')
        if (bracket_carousel) {
            new Swiper(bracket_carousel, {
                modules: [Navigation, Pagination],
                spaceBetween: 30,
                pagination: {
                    el: bracket_carousel.querySelector('.swiper-pagination'),
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1
                    },
                    640: {
                        slidesPerView: 2
                    },
                    1000: {
                        slidesPerView: 4
                    }
                }
            });
        }
        let factoring_carousel = document.querySelector('.factoring-carousel')
        if (factoring_carousel) {
            new Swiper(factoring_carousel, {
                modules: [Navigation, Pagination, Grid],
                spaceBetween: 30,
                
                pagination: {
                    el: factoring_carousel.querySelector('.swiper-pagination'),
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1,
                        grid: false
                    },
                    640: {
                        slidesPerView: 2,
                        grid: false
                    },
                    992: {
                        slidesPerView: 2,
                        grid: {
                            rows: 2,
                            fill: 'row'
                        },
                    }
                }
            });
        }

        let acdantage_carousel = document.querySelector('.acdantage-carousel')
        if (acdantage_carousel) {
            new Swiper(acdantage_carousel, {
                modules: [Navigation, Pagination],
                spaceBetween: 30,
                pagination: {
                    el: acdantage_carousel.querySelector('.swiper-pagination'),
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1
                    },
                    640: {
                        slidesPerView: 2
                    },
                    1000: {
                        slidesPerView: 3
                    }
                }
            });
        } 
    }
    MapGenerate (params, popup) {
        const city = params.mapCity
        const address = params.mapAddress
        const time = params.mapTime
        const coords = params.mapCroods
        const zoom = params.zoom

        const containers = {
            city: popup.querySelector('[data-city]'),
            address: popup.querySelector('[data-address]'),
            time: popup.querySelector('[data-time]'),
            map: popup.querySelector('[data-map]')
        }
        for (let container of Object.keys(containers)) {
            containers[container].innerHTML = ''
        }

        containers.city.innerHTML = city
        containers.address.innerHTML = address
        containers.time.innerHTML = time

        if (coords) {

            ymaps.ready(() => {  

                const map = new ymaps.Map(containers.map, {
                  center: JSON.parse(coords), 
                  zoom: Number(zoom),
                  controls: []
                });
            
                if (map) {
                    map.geoObjects.add(new ymaps.Placemark(JSON.parse(coords), {
                        balloonContent: address
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#f54e00'
                    }))
                }
            })
        }
        
    }
    Popups () {
        let topOffset = 0
        if (this.search_target) {
            const visible = () => {
                let popup = document.querySelector(this.search_target.dataset.searchTarget)
                
                if (!popup) return false

                if (popup.classList.contains('active')) {
                    popup.classList.remove('active')
                } else {
                    popup.classList.add('active')
                }
            }

            this.search_target.addEventListener('click', (e) => {
                e.preventDefault()
                visible()
            })

            document.querySelectorAll('[data-serach-popup-close]').forEach(item => {
                item.addEventListener('click', visible)
            })
        }
        if (this.popup_links) {
            this.popup_links.forEach(link => {
                link.addEventListener('click', (e) => {
                    e.preventDefault()
                    topOffset = window.pageYOffset
                    document.querySelector('body').classList.add('overflow--hidden')
                    document.querySelector('body').classList.add('fixed')
                    document.querySelector('body').style.top = `-${topOffset}px`
                    

                    let target_popup = document.querySelector(`[data-popup-target="${e.target.dataset.popup}"]`)

                    if (target_popup) {

                        if (link.dataset.value) {
                            let value = JSON.parse(link.dataset.value)
                            let names = Object.keys(value)
                            names.forEach(name => {
                                target_popup.querySelector(`[name="${name}"]`).value = value[name]
                            })
                        }
                        if (e.target.dataset.popup === 'map') {
                            this.MapGenerate(e.target.dataset, target_popup)
                        }

                        let first_input = target_popup.querySelector('input')
                        target_popup.classList.add('active')
                        if (first_input) {
                            setTimeout(() => {
                                first_input.focus({preventScroll: true})
                            }, 50)
                        }
                        setTimeout(() => {
                            target_popup.classList.add('active--scroll')
                        }, 150)
                        this.ValidateForm()
                    }
                })
            })

            document.querySelectorAll('[data-popup-close]').forEach(close => {
                close.addEventListener('click', (e) => {
                    e.preventDefault()
                    
                    document.querySelector('body').classList.remove('overflow--hidden')
                    document.querySelector('body').classList.remove('fixed')
                    
                    e.target.closest('.popup').classList.remove('active')
                    e.target.closest('.popup').classList.remove('active--scroll')
                    window.scrollTo(0, topOffset)
                    document.querySelector('body').removeAttribute('style')
                    
                })
            })

            document.addEventListener('keydown', (e) => {
                if (e.key === 'Escape' || e.key === 'Esc') {
                    e.preventDefault()
                    document.querySelector('body').classList.remove('overflow--hidden')
                    document.querySelector('body').classList.remove('fixed')

                    window.scrollTo(0, topOffset)
                    document.querySelector('body').removeAttribute('style')

                    let popups = document.querySelectorAll('.popup')
                    for (let popup of popups) {
                        popup.classList.remove('active')
                    }
                    document.querySelector('.search-popup').classList.remove('active')
                    document.querySelector('.search-popup').classList.remove('active--scroll')
                }
            })

            
        }
    }

    SlideToogle () {
        let toggles = document.querySelectorAll('[data-slide-toggle]')

        if (!toggles) return false

        toggles.forEach(item => {
            let toggle_id = item.dataset.slideToggle
            let target = document.querySelector(toggle_id)

            if (target.classList.contains('hidden-mobile') && window.matchMedia("(max-width: 767px)").matches) {
                target.style.display = 'none'
            } else {
                target.removeAttribute('style')
            }

            item.querySelector('.icon-chevron').addEventListener('click', (e) => {
                e.preventDefault()
                if (target.classList.contains('active')) {
                    item.classList.remove('active')
                    target.classList.remove('active')
                    slideToggle(target, {
                        duration: 500
                    })
                } else {
                    item.classList.add('active')
                    target.classList.add('active')
                    slideToggle(target, {
                        duration: 500
                    })
                }
            })

        })
    }

    Jobs () {
        if (this.jobs) {
            this.jobs.forEach(job => {
                job.addEventListener('click', (e) => {
                    e.preventDefault
                    let parent_job = job.nextElementSibling

                    if (parent_job.parentElement.classList.contains('active')) {
                        parent_job.parentElement.classList.remove('active')
                        slideUp(job.nextElementSibling, {
                            duration: 500
                        })
                    } else {
                        parent_job.parentElement.classList.add('active')
                        slideDown(job.nextElementSibling, {
                            duration: 500
                        })
                    }
                })
            })
        }
    }
    MaskPhone (input_phone) {
        let inputs = document.querySelectorAll('[data-phone]')
        if (inputs) {
            inputs.forEach(input => {
                IMask(input, {
                    mask: '+{7} (000) 000-00-00',
                    lazy: false,
                })
            })
        }
        
        if (input_phone) {
            let imask = IMask(input_phone, {
                mask: '+{7} (000) 000-00-00',
                lazy: false,
            })
            const valid = () => {
                if (imask._unmaskedValue.length === 11) {
                    input_phone.closest('.fieldset').classList.remove('error-active')
                    input_phone.closest('.fieldset').classList.remove('complete-phone')
                } else {
                    input_phone.closest('.fieldset').classList.add('error-active')
                }
            }
            imask.on('accept', e => {
                valid()
            })

            valid()
        }
    }
    MaskEmail (input_email) {
        let inputs = document.querySelectorAll('[data-email]')
        const validateEmail = (email) => {
            return /\S+@\S+\.\S+/.test(email);
        };

        inputs.forEach(input => {
            if (!validateEmail(input.value)) {
                input.closest('.fieldset').classList.add('error-active')
            } else {
                input.closest('.fieldset').classList.remove('error-active')
            }
        })
        
    }
    PolicyForm () {
        let policy_block = document.querySelectorAll('[data-policy]')
        if (policy_block) {
            policy_block.forEach(block => {

                let check = block.querySelector('input[type=checkbox]')
                let submit = block.querySelector('input[type=submit]')
                let checked = () => {
                    if (!check.checked) {
                        submit.setAttribute('disabled', true)
                    } else {
                        submit.removeAttribute('disabled')
                    }
                }

                checked()

                check.addEventListener('change', checked)
                
            })
        }
    }
    FileInput () {
        let files_block = document.querySelectorAll('[data-file]')
        if (files_block) {
            files_block.forEach(block => {
                let input = block.querySelector('input[type=file]')
                let names = block.querySelector('[data-names]')
                if (input) {
                    input.addEventListener('change', (e) => {
                        block.classList.add('selected')
                        names.innerHTML = Array.from(input.files).map(file => file.name).join(', ')
                    })
                }
            })
        }
    }
    ImagesWrapperLinks () {
        let contents = document.querySelectorAll('.separete__text')

        if (contents) {
            contents.forEach(content => {
                let string_html = content.innerHTML
                let replaces_content = string_html.replace(/.*(<img\s+.*src\s*=\s*"([^"]+)".*>).*/gi, (match, p1, p2) => {
                        return `<a data-fslightbox href="${p2}">${p1}</a>`
                });

                content.innerHTML = replaces_content
                refreshFsLightbox()
            })
        }
    }
    ValidateForm () {
        let forms = document.querySelectorAll('form')

        if (forms) {
            for (let form of forms) {
                let inputs = form.querySelectorAll('[data-required]')

                form.addEventListener('submit', (e) => {
                    e.preventDefault()

                    for (let input of inputs) {
                        if (!input.hasAttribute('data-phone') && !input.hasAttribute('data-email')) {
                            if (!input.value) {
                                input.closest('.fieldset').classList.add('error-active')
                                
                            } else {
                                input.closest('.fieldset').classList.remove('error-active')
                            }
                        } else if (input.hasAttribute('data-email')) {
                            this.MaskEmail(input)
                        } else {
                            this.MaskPhone(input)
                        }
                        
                    }

                })

                if (inputs) {
                    for (let input of inputs) {
                        if (input.value) {
                            input.closest('.fieldset').classList.add('focus')
                        }
                        input.addEventListener('focus', (e) => {
                            //input.closest('.popup').scrollTop = 0
                            input.closest('.fieldset').classList.add('focus')
                            input.closest('.fieldset').classList.remove('error-active')
                            
                            setTimeout(() => {
                                
                                input.scrollIntoView({block: 'center', 'behavior': 'smooth'})
                                
                            }, 70)
                        })
                        input.addEventListener('blur', () => {
                            if (!input.value) {
                                input.closest('.fieldset').classList.remove('focus')
                            }
                            input.closest('.fieldset').classList.remove('error-active')
                        })
                        input.addEventListener('input', () => {
                            input.closest('.fieldset').classList.remove('error-active')
                        })

                        if (input.hasAttribute('data-inn')) {
                            input.addEventListener('input', (e) => {
                                const limit = e.target.dataset.innLimit ? Number(e.target.dataset.innLimit) : 12
                                e.target.value = e.target.value.replace(/[^\d]+/g, '')
                                
                                if (e.target.value.length > limit) {
                                    e.target.value = e.target.value.slice(0,12)
                                }
                            })
                        }
                        
                    }
                }

                
                

            }
        }
    }
    SearchMobile () {
        let button = document.querySelector('[data-search-button]')
        let button_cancel = document.querySelector('[data-cancel]')
        let input = document.querySelector('.nav-mobile .search input')
        let topOffset = 0
        if (button) {
            button.addEventListener('click', (e) => {
                if (this.burger && this.top_line && this.nav_mobile) {
                    this.burger.classList.add('active')
                    this.top_line.classList.add('active')
                    this.nav_mobile.classList.add('active')
                    topOffset = window.pageYOffset
                    this.topOffset = window.pageYOffset
                    document.querySelector('body').classList.add('overflow--hidden')
                    document.querySelector('body').style.top = `-${topOffset}px`
                    if (topOffset !== 0) {
                        document.querySelector('body').classList.add('fixed')
                    }
                }

                if (input) {
                    input.focus()
                    input.addEventListener('input', () => {
                        if (input.value) {
                            input.closest('.search').classList.add('active')
                        } else {
                            input.closest('.search').classList.remove('active')
                        }
                    })
                }
                
            })
        }

        if (button_cancel) {
            
            button_cancel.addEventListener('click', () => {
                input.value = ''
                button_cancel.closest('.search').classList.remove('active')
            })
        }
    }
    CookiesContainer () {
        let button = document.querySelector('[data-cookies-close]')
        let container = document.querySelector('[data-cookies]')
        
        if (!Cookies.get('popup-cookie')) {
            container.classList.add('active')
        }

        button.addEventListener('click', (e) => {
            e.preventDefault()
            Cookies.set('popup-cookie', true)
            container.classList.remove('active')
        })
        
    }
    FixedTopLine () {
        if (this.site) {
            if (window.pageYOffset > 0) {
                this.site.classList.add('fixed')
            } else {
                this.site.classList.remove('fixed')
            }
        }
    }
}

new ATBLK()